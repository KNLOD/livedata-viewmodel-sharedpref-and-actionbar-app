package com.example.activityandlayoutsapp.data

import com.example.activityandlayoutsapp.R
import com.example.activityandlayoutsapp.model.RandomImage


class Datasource{
    /**
     * @loadImages - return List of model RandomImage
     *
     */
    fun loadImages() : List<RandomImage>{
      return listOf(
          RandomImage(R.drawable.space),
          RandomImage(R.drawable.field),
          RandomImage(R.drawable.waterfall)
      )
    }
}