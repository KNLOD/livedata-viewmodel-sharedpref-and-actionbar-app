package com.example.activityandlayoutsapp

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText

class NoteActivity : AppCompatActivity() {
    companion object{
        val NOTE = "note"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)
        supportActionBar?.title = getString(R.string.write_down)

        val saveButton : FloatingActionButton = findViewById(R.id.save_button)
        val noteInput : TextInputEditText = findViewById(R.id.note_input)

        val sharedPreferences = getSharedPreferences("SharedPrefs", Context.MODE_PRIVATE)
        val viewModelFactory = PersonViewModel.PersonViewModelFactory(sharedPreferences)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(PersonViewModel::class.java)

        viewModel.loadData()

        viewModel.note.observe(this, Observer{note ->
            noteInput.setText(note)
        })

        saveButton.setOnClickListener{
            val note = noteInput.text.toString()
            viewModel.saveNote(note)
        }
    }
}