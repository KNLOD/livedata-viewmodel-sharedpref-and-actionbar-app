package com.example.activityandlayoutsapp.model

import androidx.annotation.DrawableRes

/**
 * Model of Random image
 * @imageResourceId id of Drawable
 */
class RandomImage(
    @DrawableRes val imageResourceId : Int
)