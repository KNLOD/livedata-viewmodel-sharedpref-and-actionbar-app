package com.example.activityandlayoutsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ScrollView
import android.widget.TextView
import com.example.activityandlayoutsapp.MainActivity.Companion.AGE
import com.example.activityandlayoutsapp.MainActivity.Companion.FATHERS_NAME
import com.example.activityandlayoutsapp.MainActivity.Companion.FIRST_NAME
import com.example.activityandlayoutsapp.MainActivity.Companion.HOBBIES
import com.example.activityandlayoutsapp.MainActivity.Companion.LAST_NAME

class InfoActivity : AppCompatActivity() {
    /**
     * Activity for showing saved Data in MainActivity
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val bundle : Bundle? = intent?.extras

        val lastName = bundle?.get(LAST_NAME)
        val firstName = bundle?.get(FIRST_NAME)
        val fathersName = bundle?.get(FATHERS_NAME)
        val age = bundle?.get(AGE)
        val hobbies = bundle?.get(HOBBIES)

        val information: TextView = findViewById(R.id.information)

        information.text = getString(R.string.information,
            lastName,
            firstName,
            fathersName,
            age,
            hobbies
        )

    }
}