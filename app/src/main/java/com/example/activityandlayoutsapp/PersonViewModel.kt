package com.example.activityandlayoutsapp

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.activityandlayoutsapp.MainActivity.Companion.AGE
import com.example.activityandlayoutsapp.MainActivity.Companion.FATHERS_NAME
import com.example.activityandlayoutsapp.MainActivity.Companion.FIRST_NAME
import com.example.activityandlayoutsapp.MainActivity.Companion.HOBBIES
import com.example.activityandlayoutsapp.MainActivity.Companion.LAST_NAME
import com.example.activityandlayoutsapp.NoteActivity.Companion.NOTE

class PersonViewModel(private val sharedPreferences: SharedPreferences?) : ViewModel() {

    /**
     * Factory method for @PersonViewModel
     */
    class PersonViewModelFactory(private val sharedPreferences: SharedPreferences?) : ViewModelProvider.Factory{
         override fun <T : ViewModel> create(modelClass: Class<T>): T{
            return modelClass.getConstructor(SharedPreferences::class.java)
                .newInstance(sharedPreferences) as T
        }
    }
    private val editor : SharedPreferences.Editor? = sharedPreferences?.edit()

    /**
     * Initializing all LiveData values
     */
    private var _first_name = MutableLiveData<String>()
    val first_name: LiveData<String>
        get() = _first_name

    private var _last_name = MutableLiveData<String>()
    val last_name: LiveData<String>
        get() = _last_name

    private var _fathers_name = MutableLiveData<String>()
    val fathers_name: LiveData<String>
        get() = _fathers_name

    private var _age = MutableLiveData<String>()
    val age: LiveData<String>
        get() = _age

    private var _hobbies = MutableLiveData<String>()
    val hobbies: LiveData<String>
        get() = _hobbies

    private var _note = MutableLiveData<String>()
    val note: LiveData<String>
        get() = _note



    /**
     * Load saved data from SharedPreferences
     */
    fun loadData() {
        _last_name.value = sharedPreferences?.getString(LAST_NAME, null)
        _first_name.value = sharedPreferences?.getString(FIRST_NAME, null)
        _fathers_name.value = sharedPreferences?.getString(FATHERS_NAME, null)
        _age.value = sharedPreferences?.getString(AGE, null)
        _hobbies.value = sharedPreferences?.getString(HOBBIES, null)
        _note.value = sharedPreferences?.getString(NOTE, null)
    }

    /**
     * Save data in SharedPreferences
     */
    fun saveData(
        last_name: String?,
        first_name: String?,
        fathers_name: String?,
        age: String?,
        hobbies: String?,
    ) {
        val editor: SharedPreferences.Editor? = sharedPreferences?.edit()
        editor?.apply {
            putString(FIRST_NAME, first_name)
            putString(LAST_NAME, last_name)
            putString(FATHERS_NAME, fathers_name)
            putString(AGE, age)
            putString(HOBBIES, hobbies)
        }?.apply()
    }
    fun saveNote(note: String?){
        editor?.apply{
            putString(NOTE, note)
        }?.apply()
    }
}







