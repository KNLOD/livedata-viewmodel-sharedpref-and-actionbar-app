package com.example.activityandlayoutsapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.activityandlayoutsapp.data.Datasource
import com.example.activityandlayoutsapp.model.RandomImage
import com.google.android.material.textfield.TextInputEditText


class MainActivity : AppCompatActivity() {

    /**
     * Initialization of SharedPreferences and ViewModel
     */
    private val sharedPreferences : SharedPreferences? = getSharedPreferences("PersonsSharedPref", Context.MODE_PRIVATE)
    private val viewModelFactory = PersonViewModel.PersonViewModelFactory(sharedPreferences)
    private val viewModel = ViewModelProvider(this, viewModelFactory).get(PersonViewModel::class.java)

    companion object{

        val LAST_NAME = "last_name"
        val FIRST_NAME = "first name"
        val FATHERS_NAME = "fathers name"
        val AGE = "age"
        val HOBBIES = "hobbies"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title="You"

        /**
         * Initialization of Views
         */

        val last_name_TextEdit = findViewById<TextInputEditText>(R.id.last_name)
        val first_name_TextEdit = findViewById<TextInputEditText>(R.id.first_name)
        val fathers_name_TextEdit = findViewById<TextInputEditText>(R.id.fathers_name)
        val age_TextEdit = findViewById<TextInputEditText>(R.id.age)
        val hobbies_TextEdit = findViewById<TextInputEditText>(R.id.hobbies)
        val enter_button = findViewById<Button>(R.id.enter_button)


        /**
         * Setting ViewModels observers
         *
         */
        viewModel.last_name.observe(this, Observer{ first_name ->
            last_name_TextEdit.setText(first_name)
        })
        viewModel.first_name.observe(this, Observer{ last_name ->
            first_name_TextEdit.setText(last_name)
        })
        viewModel.fathers_name.observe(this, Observer{ fathers_name ->
            fathers_name_TextEdit.setText(fathers_name)
        })
        viewModel.age.observe(this, Observer{ age ->
            age_TextEdit.setText(age)
        })
        viewModel.hobbies.observe(this, Observer{ hobbies ->
            hobbies_TextEdit.setText(hobbies)
        })

        // Get Data from SharedPreferences
        viewModel.loadData()


        /**
         * Entering InfoActivity on enterButton click
         */

        val intent = Intent(this, InfoActivity::class.java)

        enter_button.setOnClickListener{
           intent.putExtras(bundleOf(
                    LAST_NAME to last_name_TextEdit.text.toString(),
                    FIRST_NAME to first_name_TextEdit.text.toString(),
                    FATHERS_NAME to fathers_name_TextEdit.text.toString(),
                    AGE to age_TextEdit.text.toString(),
                    HOBBIES to hobbies_TextEdit.text.toString()
               )
           )
            startActivity(intent)
        }
    }

    /**
     * Listening menu items selected:
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId){
            // Save State selected:
             R.id.save_state -> {
                 viewModel.saveData(
                              last_name = findViewById<TextInputEditText>(R.id.last_name).text.toString(),
                              first_name = findViewById<TextInputEditText>(R.id.first_name).text.toString(),
                              fathers_name = findViewById<TextInputEditText>(R.id.fathers_name).text.toString(),
                              age = findViewById<TextInputEditText>(R.id.age).text.toString(),
                              hobbies = findViewById<TextInputEditText>(R.id.hobbies).text.toString()
                 )
             }
            // Add Note selected:
            R.id.add_note -> {
                val intent = Intent(this, NoteActivity::class.java )
                startActivity(intent)
            }
            // Change Background Image
            R.id.change_background -> {
               changeBackground(R.id.main_constraint)
            }


        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * changes Background Image of Constraint in MainActivity
      */
    private fun changeBackground(viewId : Int){
        val view : View = findViewById(viewId)
        val image : Drawable? = ContextCompat.getDrawable(applicationContext, getRandomImageId())
        view.setBackground(image)

    }

    /**
     * Returns Random Image to be used in @changeBackground()
     */
    private fun getRandomImageId() : Int {
        val images : List<RandomImage> = Datasource().loadImages()
        val randomImage = images.random()
        return randomImage.imageResourceId

    }

}